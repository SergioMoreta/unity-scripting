﻿using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes;         // posibles ejes de escalado
    public float scaleUnits;      // velocidad de escalado

    // Update is called once per frame
    void Update()
    {
        // Acotacion de los valores de escalado al valor unitario [-1, 1]
        axes = CapsuleMovement.ClampVector3(axes);

        // La escala, al contrario que la rotacion y el movimiento, es acumulativa
        // Lo que quiere decir que  debemos añadir al nuevo valor de la escala, el valor anterior
        transform.localScale += axes * (scaleUnits * Time.deltaTime);
    }
}
